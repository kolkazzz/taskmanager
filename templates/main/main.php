<div class="row">
    <a href="./login"><button class="btn btn-primary">Вход</button></a>
</div>

<div class="row">
    <div id="TaskTableContainer" class="tableContainer"></div>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#TaskTableContainer').jtable({
                title: 'Задачи',
                paging: true, //Enable paging
                pageSize: 3, //Set page size (default: 10)
                defaultSorting: 'id DESC', //Set default sorting
                sorting: true,
                messages: {
                    addNewRecord: '+ Добавить задачу'
                },
                actions: {
                    listAction: './task/getAll',
                    createAction: './task/add',
                },
                fields: {
                    id: {
                        key: true,
                        title: 'Id',
                        width: '5%'
                    },
                    name: {
                        title: 'Имя',
                        width: '10%',
                        create: true,
                        edit: true
                    },
                    email: {
                        title: 'email',
                        width: '15%',
                        create: true,
                        edit: true
                    },
                    text: {
                        title: 'Текст задачи',
                        width: '50%',
                        create: true,
                        edit: true
                    },
                    status: {
                        title: 'Статус',
                        width: '10%',
                        create: false,
                        options: { '0': 'открыта',
                            '1': 'закрыта'
                        }
                    },
                    changed: {
                        title: 'Ред. Адм.',
                        width: '10%',
                        create: false,
                        options: { '0': '',
                            '1': 'Исправлена'
                        }
                    }
                }
            });

            $('#TaskTableContainer').jtable('load');

        });

    </script>
</div>