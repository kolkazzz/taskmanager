<div class="row">
    <div class="col col-sm-4">
        <form method="post">
            <div class="form-group">
                <label for="login">Логин</label>
                <input type="text" class="form-control" name="login" id="login" placeholder="Enter login">
            </div>
            <div class="form-group">
                <label for="Password">Пароль</label>
                <input type="password" class="form-control" name="password" id="Password" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary">Вход</button>
            <?php
                  if($hasError){
                        echo '<small id="errorMsg" class="form-text text-error">Логин или пароль не верны</small>';
                  }
            ?>
        </form>
    </div>
</div>
