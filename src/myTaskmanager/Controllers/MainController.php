<?php

namespace myTaskmanager\Controllers;


use myTaskmanager\View\View;

class MainController
{
    private $view;

    public function __construct()
    {
        $this->view = new View(__DIR__ . '/../../../templates');
    }

    public function main()
    {
        $tasks = [
            ['name' => 'Статья 1', 'text' => 'Текст статьи 1'],
            ['name' => 'Статья 2', 'text' => 'Текст статьи 2'],
        ];

        if(!empty($_SESSION['auth'])){
            $this->view->renderHtml('main/admin.php', ['tasks' => $tasks]);
        }else{
            $this->view->renderHtml('main/main.php', ['tasks' => $tasks]);
        }


    }

    public function loginAction()
    {
        $hasError = false;

        if (!empty($_POST['password']) and !empty($_POST['login'])) {
            $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_SPECIAL_CHARS);
            $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS);

            if ($login == "admin" && md5($password) == md5("123")) {
                $_SESSION['auth'] = true;
                header('Location: ./');
            } else {
                $hasError = true;
            }
        }

        $this->view->renderHtml('main/login.php', ['hasError' => $hasError]);
    }

    public function logoutAction()
    {
        unset($_SESSION['auth']);
        session_destroy();
        header('Location: ./');
    }

}
