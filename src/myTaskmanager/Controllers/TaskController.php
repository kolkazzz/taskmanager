<?php

namespace myTaskmanager\Controllers;

use myTaskmanager\Components\DB;
use myTaskmanager\Models\Tasks\Task;

class TaskController
{
    protected function selectTaskOnPage ($order)
    {
        $request = "SELECT * FROM tasks WHERE 1=1 " . $order;
        $items = DB::getAll($request);
        return $items;
    }

    protected function totalCount ()
    {
        $request = "SELECT count(id) FROM tasks";
        $count = DB::getAll($request);
        return (int) $count;
    }

    public function taskAction(string $name)
    {
        if(in_array($name, ['getAll', 'add', 'upd', 'del'])){
            $this->$name();
            return true;
        }

        $jTableResult = array();
        $jTableResult['Result'] = "ERROR";
        $jTableResult['Message'] = "Неизвестный метод";
        echo json_encode($jTableResult);
        return false;
    }

    protected function add()
    {
        $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
        $text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_SPECIAL_CHARS);
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);

        $task = new Task([0, $name, $email, $text, 0, 0]);

        if($task->validate()){
            $insert_id = DB::add("INSERT INTO `tasks` SET `name` = ?, `email` = ?, `text` = ?", [$name, $email, $text]);
            if($insert_id == 0){
                $message = "Ошибка добавления";
            }else{
                $item = DB::getRow("SELECT * FROM `tasks` WHERE `id` = ?", $insert_id);
            }

        }else{
            $result = false;
            $message = "Неправильный email";
        }

        $jTableResult = array();
        $jTableResult['Result'] = $item ? "OK" : "ERROR";
        if(!$item && $message) $jTableResult['Message'] = $message;
        if($item && is_array($item)) $jTableResult['Record'] = $item;
        echo json_encode($jTableResult);
        return true;

    }

    protected function del()
    {
        if (!empty($_SESSION['auth'])) {
            if (isset($_POST['id'])) {
                $idTask = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
                $result = DB::set("DELETE FROM `tasks` WHERE `id` = ?", [$idTask]);
            }

            $jTableResult = array();
            $jTableResult['Result'] = $result ? "OK" : "ERROR";
            echo json_encode($jTableResult);
            return true;
        }
    }

    protected function upd()
    {
        if(!empty($_SESSION['auth'])) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $text = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
            $status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT);

            if (isset($_POST['id'])) {
                $idTask = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

                $item = DB::getRow("SELECT * FROM `tasks` WHERE `id` = ?", [$idTask]);

                if ($item) {
                    $task = new Task($item);

                    $task->setName($name);
                    $task->setEmail($email);
                    $task->setText($text);
                    $task->setStatus($status);

                    if ($task->validate()) {
                        $newItem = $task->getTask();
                        $result = DB::set("UPDATE `tasks` SET name = :name, email = :email, text = :text, status = :status, changed = :changed  WHERE `id` = :id", $newItem);
                    } else {
                        $message = "Данные не валидны";
                    }

                } else {
                    $message = "Записи уже не существует";
                }

            }
        }else{
            $result = false;
            $message = "Действие запрещено";
        }

        $jTableResult = array();
        $jTableResult['Result'] = $result ? "OK" : "ERROR";
        if (!$result && $message) $jTableResult['Message'] = $message;
        if ($newItem && is_array($newItem)) $jTableResult['Record'] = $newItem;

        echo json_encode($jTableResult);
        return true;

    }

    protected function getAll(){
        $jtSorting = filter_input(INPUT_GET, 'jtSorting', FILTER_SANITIZE_STRING);
        $order = $jtSorting != '' ? " ORDER BY " .$jtSorting : " ORDER BY id DESC";

        if(isset($_REQUEST['jtStartIndex']) || isset($_REQUEST['jtPageSize'])) {
            $jtStartIndex = filter_input(INPUT_GET, 'jtStartIndex', FILTER_VALIDATE_INT);
            $jtPageSize = filter_input(INPUT_GET, 'jtPageSize', FILTER_VALIDATE_INT);
            $jtPageSize = $jtPageSize == 0 ? 3 : $jtPageSize;
            $order .= " LIMIT " . $jtStartIndex . ", " . $jtPageSize;
        }

        $itemsOnPage = $this->selectTaskOnPage($order);

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $this->totalCount();
        $jTableResult['Records'] = $itemsOnPage;
        echo json_encode($jTableResult);
        return true;
    }


}
