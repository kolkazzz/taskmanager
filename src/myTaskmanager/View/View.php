<?php

namespace myTaskmanager\View;

class View
{
    private $templatesPath;

    public function __construct(string $templatesPath)
    {
        $this->templatesPath = $templatesPath;
    }

    public function renderHtml(string $templateName, array $vars = [])
    {
        extract($vars);

        ob_start();
        include $this->templatesPath . '/header.php';
        include $this->templatesPath . '/' . $templateName;
        include $this->templatesPath . '/footer.php';
        $buffer = ob_get_contents();
        ob_end_clean();

        echo $buffer;
    }
}