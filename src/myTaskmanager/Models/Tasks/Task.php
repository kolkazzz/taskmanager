<?php

namespace myTaskmanager\Models\Tasks;

class Task
{
    private $id;
    private $name;
    private $text;
    private $email;
    private $status;
    private $changed;


    public function __construct($task = array())
    {
        [$this->id, $this->name, $this->email, $this->text, $this->status, $this->changed] = array_values($task);
    }

    public function getTask(): array
    {
        return ['id' =>$this->id,
                'name' => $this->name,
                'email' => $this->email,
                'text' => $this->text,
                'status' => $this->status,
                'changed' => $this->changed];
    }

    public function setName($name): bool
    {
        $this->name = $name;
        return true;
    }

    public function setEmail($email): bool
    {
        $this->email = $email;
        return true;
    }

    public function setText($text): bool
    {
        if($this->text != $text) $this->changed = 1;
        $this->text = $text;
        return true;
    }

    public function setStatus($status): bool
    {
        $this->status = $status;
        return true;
    }

    public function validate(): bool
    {
        $isValid = true;

        if($this->name == "" || strlen ($this->name)>50) $isValid = false;

        if($this->text == "" || strlen ($this->name)>500) $isValid = false;

        if($this->email == "" || !filter_var($this->email, FILTER_VALIDATE_EMAIL)) $isValid = false;

        return $isValid;
    }

}
