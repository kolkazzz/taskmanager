<?php

return [
    '~^task/(.*)$~' => [\myTaskmanager\Controllers\TaskController::class, 'taskAction'],
    '~^login$~' => [\myTaskmanager\Controllers\MainController::class, 'loginAction'],
    '~^logout$~' => [\myTaskmanager\Controllers\MainController::class, 'logoutAction'],
    '~^$~' => [\myTaskmanager\Controllers\MainController::class, 'main'],
];
